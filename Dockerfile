FROM mcr.microsoft.com/dotnet/core/sdk

WORKDIR /app

RUN dotnet new -i Piranha.BasicWeb.CSharp
RUN dotnet new -i Piranha.Blog.CSharp

RUN dotnet new piranha
RUN dotnet restore


EXPOSE 80

CMD ["dotnet", "run"]